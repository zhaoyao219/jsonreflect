/*
  Copyright (c) 2009 Dave Gamble
 
  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:
 
  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.
 
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.
*/

#ifndef cJSON__h
#define cJSON__h

#ifdef __cplusplus

#include "reflect.h"

extern "C"
{
#endif

/* cJSON Types: */
#define cJSON_False 0
#define cJSON_True 1
#define cJSON_NULL 2
#define cJSON_Number 3
#define cJSON_String 4
#define cJSON_Array 5
#define cJSON_Object 6
	
#define cJSON_IsReference 256
#define cJSON_StringIsConst 512

/* The cJSON structure: */
typedef struct cJSON {
	struct cJSON *next,*prev;	/* next/prev allow you to walk array/object chains. Alternatively, use GetArraySize/GetArrayItem/GetObjectItem */
	struct cJSON *child;		/* An array or object item will have a child pointer pointing to a chain of the items in the array/object. */

	int type;					/* The type of the item, as above. */

	char *valuestring;			/* The item's string, if type==cJSON_String */
	int valueint;				/* The item's number, if type==cJSON_Number */
	double valuedouble;			/* The item's number, if type==cJSON_Number */

	char *string;				/* The item's name string, if this item is the child of, or is in the list of subitems of an object. */
} cJSON;

typedef struct cJSON_Hooks {
	  void *(*malloc_fn)(size_t sz);
	  void (*free_fn)(void *ptr);
} cJSON_Hooks;

/* Supply malloc, realloc and free functions to cJSON */
extern void cJSON_InitHooks(cJSON_Hooks* hooks);


/* Supply a block of JSON, and this returns a cJSON object you can interrogate. Call cJSON_Delete when finished. */
extern cJSON *cJSON_Parse(const char *value);
/* Render a cJSON entity to text for transfer/storage. Free the char* when finished. */
extern char  *cJSON_Print(cJSON *item);
/* Render a cJSON entity to text for transfer/storage without any formatting. Free the char* when finished. */
extern char  *cJSON_PrintUnformatted(cJSON *item);
/* Render a cJSON entity to text using a buffered strategy. prebuffer is a guess at the final size. guessing well reduces reallocation. fmt=0 gives unformatted, =1 gives formatted */
extern char *cJSON_PrintBuffered(cJSON *item,int prebuffer,int fmt);
/* Delete a cJSON entity and all subentities. */
extern void   cJSON_Delete(cJSON *c);

/* Returns the number of items in an array (or object). */
extern int	  cJSON_GetArraySize(cJSON *array);
/* Retrieve item number "item" from array "array". Returns NULL if unsuccessful. */
extern cJSON *cJSON_GetArrayItem(cJSON *array,int item);
/* Get item "string" from object. Case insensitive. */
extern cJSON *cJSON_GetObjectItem(cJSON *object,const char *string);

/* For analysing failed parses. This returns a pointer to the parse error. You'll probably need to look a few chars back to make sense of it. Defined when cJSON_Parse() returns 0. 0 when cJSON_Parse() succeeds. */
extern const char *cJSON_GetErrorPtr(void);
	
/* These calls create a cJSON item of the appropriate type. */
extern cJSON *cJSON_CreateNull(void);
extern cJSON *cJSON_CreateTrue(void);
extern cJSON *cJSON_CreateFalse(void);
extern cJSON *cJSON_CreateBool(int b);
extern cJSON *cJSON_CreateNumber(double num);
extern cJSON *cJSON_CreateString(const char *string);
extern cJSON *cJSON_CreateArray(void);
extern cJSON *cJSON_CreateObject(void);

/* These utilities create an Array of count items. */
extern cJSON *cJSON_CreateIntArray(const int *numbers,int count);
extern cJSON *cJSON_CreateFloatArray(const float *numbers,int count);
extern cJSON *cJSON_CreateDoubleArray(const double *numbers,int count);
extern cJSON *cJSON_CreateStringArray(const char **strings,int count);

/* Append item to the specified array/object. */
extern void cJSON_AddItemToArray(cJSON *array, cJSON *item);
extern void	cJSON_AddItemToObject(cJSON *object,const char *string,cJSON *item);
extern void	cJSON_AddItemToObjectCS(cJSON *object,const char *string,cJSON *item);	/* Use this when string is definitely const (i.e. a literal, or as good as), and will definitely survive the cJSON object */
/* Append reference to item to the specified array/object. Use this when you want to add an existing cJSON to a new cJSON, but don't want to corrupt your existing cJSON. */
extern void cJSON_AddItemReferenceToArray(cJSON *array, cJSON *item);
extern void	cJSON_AddItemReferenceToObject(cJSON *object,const char *string,cJSON *item);

/* Remove/Detatch items from Arrays/Objects. */
extern cJSON *cJSON_DetachItemFromArray(cJSON *array,int which);
extern void   cJSON_DeleteItemFromArray(cJSON *array,int which);
extern cJSON *cJSON_DetachItemFromObject(cJSON *object,const char *string);
extern void   cJSON_DeleteItemFromObject(cJSON *object,const char *string);
	
/* Update array items. */
extern void cJSON_InsertItemInArray(cJSON *array,int which,cJSON *newitem);	/* Shifts pre-existing items to the right. */
extern void cJSON_ReplaceItemInArray(cJSON *array,int which,cJSON *newitem);
extern void cJSON_ReplaceItemInObject(cJSON *object,const char *string,cJSON *newitem);

/* Duplicate a cJSON item */
extern cJSON *cJSON_Duplicate(cJSON *item,int recurse);
/* Duplicate will create a new, identical cJSON item to the one you pass, in new memory that will
need to be released. With recurse!=0, it will duplicate any children connected to the item.
The item->next and ->prev pointers are always zero on return from Duplicate. */

/* ParseWithOpts allows you to require (and check) that the JSON is null terminated, and to retrieve the pointer to the final byte parsed. */
extern cJSON *cJSON_ParseWithOpts(const char *value,const char **return_parse_end,int require_null_terminated);

extern void cJSON_Minify(char *json);

/* Macros for creating things quickly. */
#define cJSON_AddNullToObject(object,name)		cJSON_AddItemToObject(object, name, cJSON_CreateNull())
#define cJSON_AddTrueToObject(object,name)		cJSON_AddItemToObject(object, name, cJSON_CreateTrue())
#define cJSON_AddFalseToObject(object,name)		cJSON_AddItemToObject(object, name, cJSON_CreateFalse())
#define cJSON_AddBoolToObject(object,name,b)	cJSON_AddItemToObject(object, name, cJSON_CreateBool(b))
#define cJSON_AddNumberToObject(object,name,n)	cJSON_AddItemToObject(object, name, cJSON_CreateNumber(n))
#define cJSON_AddStringToObject(object,name,s)	cJSON_AddItemToObject(object, name, cJSON_CreateString(s))

/* When assigning an integer value, it needs to be propagated to valuedouble too. */
#define cJSON_SetIntValue(object,val)			((object)?(object)->valueint=(object)->valuedouble=(val):(val))
#define cJSON_SetNumberValue(object,val)		((object)?(object)->valueint=(object)->valuedouble=(val):(val))


///////////////////////////////////////////////////////////////////////////////////////////////////////

typedef cJSON stJsonItem;

void cJSON_delete(void* ptr);

char* cJSON_strdup(const char* str);

const char* cJSON_load(cJSON* item, const char* str);

typedef enum
{
	eJSON_NULL = cJSON_NULL,
	eJSON_TRUE = cJSON_True,
	eJSON_FALSE = cJSON_False,
	eJSON_ARRAY = cJSON_Array,
	eJSON_OBJECT = cJSON_Object,
	eJSON_NUMBER = cJSON_Number,
	eJSON_STRING = cJSON_String
} E_JSON_TYPE;

#define JsonParse								cJSON_Parse
#define JsonCreate								cJSON_CreateObject
#define JsonDestroy								cJSON_Delete

#define JsonCreateNull							cJSON_CreateNull
#define JsonCreateBool							cJSON_CreateBool
#define JsonCreateNumber						cJSON_CreateNumber
#define JsonCreateString						cJSON_CreateString
#define JsonGetErrorString						cJSON_GetErrorPtr

#define JsonPrint								cJSON_PrintUnformatted
#define JsonArraySize							cJSON_GetArraySize
#define JsonGet(obj,name)						cJSON_GetObjectItem(obj,name)
#define JsonArrayGet(arr,idx)					cJSON_GetArrayItem(arr,idx)

#define JsonArrayAdd(arr,val)					cJSON_AddItemToArray(arr,val)
#define JsonArrayAddNull(obj)					cJSON_AddItemToArray(obj,cJSON_CreateNull())
#define JsonArrayAddTrue(obj)					cJSON_AddItemToArray(obj,cJSON_CreateTrue())
#define JsonArrayAddFalse(obj)					cJSON_AddItemToArray(obj,cJSON_CreateFalse())
#define JsonArrayRemove(arr,idx)				cJSON_DeleteItemFromArray(arr,idx)
#define JsonArrayAddBool(obj,val)				cJSON_AddItemToArray(obj,cJSON_CreateBool(val))
#define JsonArrayAddNumber(obj,val)				cJSON_AddItemToArray(obj,cJSON_CreateNumber(val))
#define JsonArrayAddNumber(obj,val)				cJSON_AddItemToArray(obj,cJSON_CreateNumber(val))
#define JsonArrayAddString(obj,val)				cJSON_AddItemToArray(obj,cJSON_CreateString(val))

#define JsonRemove(obj,name)					cJSON_DeleteItemFromObject(obj,name)
#define JsonAdd(obj,name,val)					cJSON_AddItemToObject(obj,name,val)
#define JsonAddNull(obj,name)					cJSON_AddItemToObject(obj,name,cJSON_CreateNull())
#define JsonAddTrue(obj,name)					cJSON_AddItemToObject(obj,name,cJSON_CreateTrue())
#define JsonAddFalse(obj,name)					cJSON_AddItemToObject(obj,name,cJSON_CreateFalse())
#define JsonAddBool(obj,name,val)				cJSON_AddItemToObject(obj,name,cJSON_CreateBool(val))
#define JsonAddNumber(obj,name,val)				cJSON_AddItemToObject(obj,name,cJSON_CreateNumber(val))
#define JsonAddNumber(obj,name,val)				cJSON_AddItemToObject(obj,name,cJSON_CreateNumber(val))
#define JsonAddString(obj,name,val)				cJSON_AddItemToObject(obj,name,cJSON_CreateString(val))


///////////////////////////////////////////////////////////////////////////////////////////////////////


static char* JsonToString(stJsonItem* item, char* msg, int sz)
{
	char* tmp = JsonPrint(item);

	return tmp ? (strncpy(msg, tmp, --sz), cJSON_delete(tmp), msg[sz] = 0, msg) : NULL;
}

static stJsonItem* JsonAddArray(stJsonItem* item, const char* name)
{
	stJsonItem* obj = cJSON_CreateArray();

	return obj ? (JsonAdd(item, name, obj), obj) : NULL;
}

static int JsonGetInteger(stJsonItem* item, const char* name)
{
	return (item = JsonGet(item, name)) ? item->valueint : 0;
}

static double JsonGetNumber(stJsonItem* item, const char* name)
{
	return (item = JsonGet(item, name)) ? item->valuedouble : 0;
}

static const char* JsonGetString(stJsonItem* item, const char* name)
{
	return (item = JsonGet(item, name)) ? item->valuestring : NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif


///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __cplusplus

class JsonElement : public Object
{
public:
	class iterator
	{
		friend class JsonElement;

	private:
		int idx;
		int len;
		stJsonItem* elem;
		sp<stJsonItem> holder;

		iterator(stJsonItem* item, JsonElement* parent)
		{
			idx = -1;
			len = -1;

			if (elem = item)
			{
				if (elem->type == eJSON_ARRAY)
				{
					if ((len = JsonArraySize(elem)) > 0) idx = 0;
				}
				else if (elem->child)
				{
					idx = 0;
					elem = elem->child;

					while (elem->prev) elem = elem->prev;
				}
			}

			if (parent) holder = parent->holder;
		}

	public:
		iterator& operator ++ ()
		{
			++idx;

			if (len < 0)
			{
				if ((elem = elem->next) == NULL) idx = -1;
			}
			else
			{
				if (idx >= len) idx = -1;
			}

			return *this;
		}
		JsonElement operator * ()
		{
			stJsonItem* item = elem;

			if (len >= 0) item = JsonArrayGet(elem, idx);

			JsonElement res(item, NULL);

			res.holder = holder;

			return res;
		}
		bool operator != (const iterator& obj) const
		{
			return idx != obj.idx;
		}
	};

protected:
	stJsonItem* elem;
	sp<stJsonItem> holder;

protected:
	static bool InitElement(stJsonItem* item)
	{
		CHECK_FALSE_RETURN(item);
		
		assert(item->child == NULL);

		if (item->valuestring)
		{
			cJSON_delete(item->valuestring);
			item->valuestring = NULL;
		}

		return true;
	}
	static bool InitElement(stJsonItem* item, int val)
	{
		CHECK_FALSE_RETURN(InitElement(item));
		
		item->type = eJSON_NUMBER;
		item->valuedouble = val;
		item->valueint = val;
		
		return true;
	}
	static bool InitElement(stJsonItem* item, bool val)
	{
		CHECK_FALSE_RETURN(InitElement(item));
		
		item->type = val ? eJSON_TRUE : eJSON_FALSE;
		
		return true;
	}
	static bool InitElement(stJsonItem* item, double val)
	{
		CHECK_FALSE_RETURN(InitElement(item));
		
		item->type = eJSON_NUMBER;
		item->valueint = (int)(val);
		item->valuedouble = val;

		return true;
	}
	static bool InitElement(stJsonItem* item, const char* val)
	{
		CHECK_FALSE_RETURN(InitElement(item));
		
		item->type = eJSON_STRING;
		item->valuestring = cJSON_strdup(val);
		
		return item->valuestring ? true : false;
	}

public:
	JsonElement()
	{
		elem = NULL;
	}
	JsonElement(const string& msg)
	{
		elem = NULL;
		init(msg);
	}
	JsonElement(stJsonItem* item, const JsonElement* parent)
	{
		if (parent) holder = parent->holder;
		elem = item;
	}

public:
	iterator end()
	{
		return iterator(NULL, this);
	}
	iterator begin()
	{
		return iterator(elem, this);
	}
	string getName() const
	{
		return elem ? elem->string : "";
	}
	string toString() const
	{
		char* tmp;
		string res;

		if (elem == NULL) return "";

		return (tmp = JsonPrint(elem)) ? (res = tmp, cJSON_delete(tmp), res) : res;
	}
	bool init(const string& msg = "")
	{
		if (elem) return load(msg);

		elem = msg.empty() ? JsonCreate() : JsonParse(msg.c_str());

		if (elem) holder = sp<stJsonItem>(elem, JsonDestroy);

		return true;
	}
	JsonElement pack(stJsonItem* item) const
	{
		return JsonElement(item, this);
	}
	char* toString(char* dest, int sz) const
	{
		return elem ? JsonToString(elem, dest, sz) : NULL;
	}

public:
	bool isNull() const
	{
		return elem == NULL || elem->type == eJSON_NULL;
	}
	bool isArray() const
	{
		return elem && elem->type == eJSON_ARRAY;
	}
	bool isObject() const
	{
		return elem && elem->type == eJSON_OBJECT;
	}
	bool isNumber() const
	{
		return elem && elem->type == eJSON_NUMBER;
	}
	bool isString() const
	{
		return elem && elem->type == eJSON_STRING;
	}
	bool isBoolean() const
	{
		return elem && (elem->type == eJSON_TRUE || elem->type == eJSON_FALSE);
	}

	bool isNull(int idx) const
	{
		return get(idx).isNull();
	}
	bool isArray(int idx) const
	{
		return get(idx).isArray();
	}
	bool isObject(int idx) const
	{
		return get(idx).isObject();
	}
	bool isNumber(int idx) const
	{
		return get(idx).isNumber();
	}
	bool isString(int idx) const
	{
		return get(idx).isString();
	}
	bool isBoolean(int idx) const
	{
		return get(idx).isBoolean();
	}

	bool isNull(const string& name) const
	{
		return get(name).isNull();
	}
	bool isArray(const string& name) const
	{
		return get(name).isArray();
	}
	bool isObject(const string& name) const
	{
		return get(name).isObject();
	}
	bool isNumber(const string& name) const
	{
		return get(name).isNumber();
	}
	bool isString(const string& name) const
	{
		return get(name).isString();
	}
	bool isBoolean(const string& name) const
	{
		return get(name).isBoolean();
	}

public:
	JsonElement add()
	{
		if (elem == NULL) init();

		stJsonItem* item = cJSON_CreateObject();

		if (item) JsonArrayAdd(elem, item);

		return pack(item);
	}
	void remove(int idx)
	{
		JsonArrayRemove(elem, idx);
	}
	bool load(const string& msg)
	{
		if (elem)
		{
			if (elem->valuestring)
			{
				cJSON_delete(elem->valuestring);
				elem->valuestring = NULL;
			}

			if (elem->child)
			{
				JsonDestroy(elem->child);
				elem->child = NULL;
			}

			if (msg.empty())
			{
				elem->type = eJSON_NULL;
				holder = NULL;
				elem = NULL;

				return true;
			}
		}
		else
		{
			if (msg.empty()) return true;

			CHECK_FALSE_RETURN(init());
		}

		return cJSON_load(elem, msg.c_str()) ? true : false;
	}
	void remove(const string& name)
	{
		JsonRemove(elem, name.c_str());
	}
	JsonElement get(int idx) const
	{
		if (elem == NULL) return pack(NULL);

		if (idx < 0 || idx >= JsonArraySize(elem)) return pack(NULL);

		return pack(JsonArrayGet(elem, idx));
	}
	JsonElement add(const string& name)
	{
		if (elem == NULL)
		{
			init();
		}
		else
		{
			JsonElement tmp = get(name);

			if (tmp.elem) return tmp;
		}

		stJsonItem* item = cJSON_CreateNull();

		if (item) JsonAdd(elem, name.c_str(), item);

		return pack(item);
	}
	JsonElement addArray(const string& name)
	{
		if (elem == NULL)
		{
			init();
		}
		else
		{
			JsonElement tmp = get(name);

			if (tmp.elem) return tmp;
		}

		stJsonItem* item = cJSON_CreateArray();

		if (item) JsonAdd(elem, name.c_str(), item);

		return pack(item);
	}
	JsonElement get(const string& name) const
	{
		if (elem == NULL) return pack(NULL);

		return pack(JsonGet(elem, name.c_str()));
	}

public:
	bool setVariable(bool val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(int val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(double val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(const char* val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(const string& val)
	{
		return InitElement(elem, val.c_str());
	}

public:
	string getVariable() const
	{
		if (elem)
		{
			if (elem->type == eJSON_TRUE) return "true";
			if (elem->type == eJSON_FALSE) return "false";
			if (elem->valuestring) return elem->valuestring;
			if (elem->type == eJSON_NUMBER) return to_string(elem->valuedouble);
		}
		
		return "";
	}
	bool getVariable(bool& val) const
	{
		CHECK_FALSE_RETURN(elem && (elem->type == eJSON_TRUE || elem->type == eJSON_FALSE));
		
		val = (elem->type == eJSON_TRUE);
		
		return true;
	}
	bool getVariable(int& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->type == eJSON_NUMBER);
		
		val = elem->valueint;
		
		return true;
	}
	bool getVariable(double& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->type == eJSON_NUMBER);
		
		val = elem->valuedouble;
		
		return true;
	}
	bool getVariable(string& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->valuestring);
		
		val = elem->valuestring;
		
		return true;
	}

public:
	int asInteger() const
	{
		int val = 0;

		if (getVariable(val)) return val;

		return atoi(getVariable().c_str());
	}
	bool asBoolean() const
	{
		bool val = false;

		if (getVariable(val)) return val;

		string str = getVariable();

		if (str.empty()) return false;

		if (atoi(str.c_str())) return true;

		return strcasecmp(str.c_str(), "true") == 0;
	}
	double asNumber() const
	{
		double val = 0;

		if (getVariable(val)) return val;

		return atof(getVariable().c_str());
	}
	string asString() const
	{
		return getVariable();
	}

	int asInteger(int idx) const
	{
		return get(idx).asInteger();
	}
	bool asBoolean(int idx) const
	{
		return get(idx).asBoolean();
	}
	double asNumber(int idx) const
	{
		return get(idx).asNumber();
	}
	string asString(int idx) const
	{
		return get(idx).asString();
	}

	int asInteger(const string& name) const
	{
		return get(name).asInteger();
	}
	bool asBoolean(const string& name) const
	{
		return get(name).asBoolean();
	}
	double asNumber(const string& name) const
	{
		return get(name).asNumber();
	}
	string asString(const string& name) const
	{
		return get(name).asString();
	}

public:
	int size() const
	{
		if (isArray()) return JsonArraySize(elem);
		
		if (elem->child)
		{
			stJsonItem* child = elem->child;

			if (child)
			{
				int cnt = 1;

				while (child = child->prev) cnt++;

				child = elem->child;

				while (child = child->next) cnt++;

				return cnt;
			}
		}

		return 0;
	}
	JsonElement operator [] (int idx)
	{
		JsonElement item = get(idx);

		if (item.elem) return item;

		if (elem == NULL) init();

		int sz = size();
		
		if (sz == 0)
		{
			assert(isArray() || elem->child == NULL);
		
			if (elem->valuestring)
			{
				cJSON_delete(elem->valuestring);
				elem->valuestring = NULL;
			}

			elem->type = eJSON_ARRAY;
		}
		
		if (isArray())
		{
			while (sz++ <= idx) item = add();
		}
		
		return item;
	}
	JsonElement operator [] (const string& name)
	{
		JsonElement item = get(name);
		
		return item.elem ? item : add(name);
	}

public:
	JsonElement& operator = (int val)
	{
		setVariable(val);

		return *this;
	}
	JsonElement& operator = (bool val)
	{
		setVariable(val);

		return *this;
	}
	JsonElement& operator = (double val)
	{
		setVariable(val);

		return *this;
	}
	JsonElement& operator = (const char* val)
	{
		setVariable(val);

		return *this;
	}
	JsonElement& operator = (const string& val)
	{
		setVariable(val);

		return *this;
	}
	JsonElement& operator = (const JsonElement& val)
	{
		holder = val.holder;
		elem = val.elem;

		return *this;
	}
};

static string& operator += (string& str, const JsonElement& obj)
{
	return str += obj.toString();
}

static ostream& operator << (ostream& out, const JsonElement& obj)
{
	return out << obj.toString();
}

static string operator + (const string& str, const JsonElement& obj)
{
	return str + obj.toString();
}

class JsonReflect : public Object
{
public:
	virtual string toString() const
	{
		const char* type;
		JsonElement data;
		vector<ReflectItem> vec = ReflectHelper::GetAttrList(this);

		for (auto& item : vec)
		{
			type = item.type;

			if (strcmp(type, "object") == 0)
			{
				JsonReflect* obj = (JsonReflect*)((char*)(this) + item.offset);

				data[item.name].load(obj->toString());
			}
			else
			{
				string val = item.get(this);

				if (strcmp(type, "string") == 0)
				{
					data[item.name] = val;
				}
				else
				{
					if (val.empty()) continue;

					if (strcmp(type, "int") == 0)
					{
						data[item.name] = atoi(val.c_str());
					}
					else if (strcmp(type, "bool") == 0)
					{
						data[item.name] = strcasecmp(val.c_str(), "true") == 0;
					}
					else
					{
						data[item.name] = atof(val.c_str());
					}
				}
			}
		}

		return data.toString();
	}
	virtual bool fromString(const string& msg)
	{
		const char* type;
		JsonElement data(msg);

		if (data.isNull()) return true;

		vector<ReflectItem> vec = ReflectHelper::GetAttrList(this);

		CHECK_FALSE_RETURN(data.isObject());

		for (auto& item : vec)
		{
			JsonElement src = data.get(item.name);

			if (src.isNull()) continue;

			type = item.type;

			if (strcmp(type, "object") == 0)
			{
				JsonReflect* obj = (JsonReflect*)((char*)(this) + item.offset);

				CHECK_FALSE_RETURN(obj->fromString(src.toString()));
			}
			else
			{
				string val = src.getVariable();

				if (strcmp(type, "string") == 0)
				{
					CHECK_FALSE_RETURN(item.set(this, val));
				}
				else
				{
					if (val.length() > 0) CHECK_FALSE_RETURN(item.set(this, val));
				}
			}
		}

		return true;
	}
	virtual bool fromObject(const JsonReflect& obj)
	{
		const char* type;
		const char* name;
		vector<ReflectItem> vec = ReflectHelper::GetAttrList(this);
		vector<ReflectItem> attrs = ReflectHelper::GetAttrList(&obj);

		for (auto& item : vec)
		{
			type = item.type;
			name = item.name;

			if (strcmp(type, "object") == 0)
			{
				JsonReflect* dest = dynamic_cast<JsonReflect*>((Object*)((char*)(this) + item.offset));

				if (dest)
				{
					for (auto& item : attrs)
					{
						if (strcmp(name, item.name) == 0 && strcmp(type, item.type) == 0)
						{
							JsonReflect* src = dynamic_cast<JsonReflect*>((Object*)((char*)(&obj) + item.offset));

							if (src) CHECK_FALSE_RETURN(dest->fromObject(*src));

							break;
						}
					}
				}
			}
			else
			{
				for (auto& item : attrs)
				{
					if (strcmp(name, item.name) == 0)
					{
						string val = item.get(&obj);

						if (strcmp(type, "string") == 0)
						{
							CHECK_FALSE_RETURN(item.set(this, val));
						}
						else
						{
							if (val.length() > 0) CHECK_FALSE_RETURN(item.set(this, val));
						}

						break;
					}
				}
			}
		}

		return true;
	}
};

template <class DATA_TYPE>
class JsonReflectList : public JsonReflect
{
protected:
	robject(vector<sp<DATA_TYPE>>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	sp<DATA_TYPE> add()
	{
		sp<DATA_TYPE> item = newsp<DATA_TYPE>();

		if (item) vec.push_back(item);

		return item;
	}
	void add(sp<DATA_TYPE> val)
	{
		vec.push_back(val);
	}
	sp<DATA_TYPE> get(int idx) const
	{
		return vec[idx];
	}
	const vector<sp<DATA_TYPE>>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++].load(item->toString());

		return res.toString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src)
		{
			sp<DATA_TYPE> dest = newsp<DATA_TYPE>();

			CHECK_FALSE_RETURN(dest);

			if (item.isString() || item.isArray() || item.isObject())
			{
				CHECK_FALSE_RETURN(dest->fromString(item.toString()));
			}
			else
			{
				CHECK_FALSE_RETURN(dest->fromString(item.getVariable()));
			}

			vec.push_back(dest);
		}

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<int> : public JsonReflect
{
protected:
	robject(vector<int>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	void add(int val)
	{
		vec.push_back(val);
	}
	size_t size() const
	{
		return vec.size();
	}
	int get(int idx) const
	{
		return vec[idx];
	}
	const vector<int>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asInteger());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<bool> : public JsonReflect
{
protected:
	robject(vector<bool>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	void add(bool val)
	{
		vec.push_back(val);
	}
	size_t size() const
	{
		return vec.size();
	}
	bool get(int idx) const
	{
		return vec[idx];
	}
	const vector<bool>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asBoolean());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<float> : public JsonReflect
{
protected:
	robject(vector<float>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	void add(float val)
	{
		vec.push_back(val);
	}
	float get(int idx) const
	{
		return vec[idx];
	}
	const vector<float>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asNumber());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<double> : public JsonReflect
{
protected:
	robject(vector<double>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	void add(double val)
	{
		vec.push_back(val);
	}
	double get(int idx) const
	{
		return vec[idx];
	}
	const vector<double>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asNumber());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<string> : public JsonReflect
{
protected:
	robject(vector<string>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	void add(const string& val)
	{
		vec.push_back(val);
	}
	string get(int idx) const
	{
		return vec[idx];
	}
	const vector<string>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto& item : vec) res[idx++] = item;

		return res.toString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.getVariable());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

#define JsonEntity(clazz) class clazz : public JsonReflect

#define rarray(type, name) reflect_object(JsonReflectList<type>, name)

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif
