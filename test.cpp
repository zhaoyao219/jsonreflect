#include "json.h"

JsonEntity(Item)
{
public:
	//定义名为ival的int类型成员
	rint(ival);
	//定义名为bval的bool类型成员
	rbool(bval);
	//定义名为sval的string类型成员
	rstring(sval);
};

JsonEntity(Entity)
{
public:
	//定义名为ival的int类型成员
	rint(ival);
	//定义名为bval的bool类型成员
	rbool(bval);
	//定义名为sval的string类型成员
	rstring(sval);
	//定义名为list的vector<Item>类型成员
	rarray(Item, list);
};

int main(int argc, char** argv)
{
	Entity obj;
	Entity tmp;
	sp<Item> item;
	
	obj.ival = 0;
	obj.bval = false;
	obj.sval = "zero";

	item = obj.list.add();
	item->ival = 1;
	item->bval = true;
	item->sval = "one";

	item = obj.list.add();
	item->ival = 2;
	item->bval = true;
	item->sval = "two";

	item = obj.list.add();
	item->ival = 3;
	item->bval = true;
	item->sval = "three";

	//对象序列化为JSON字符串
	cout << obj.toString() << endl << endl;

	cout << endl;

	//JSON字符串反序列化为对象
	tmp.fromString(obj.toString());

	cout << tmp.toString() << endl << endl;

	return 0;
}